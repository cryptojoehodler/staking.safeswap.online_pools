// New chain info for WAGMI can be found https://github.com/wevm/viem/tree/main/src/chains/definitions

const bsc = {
  id: 56,
  name: 'BNB Smart Chain',
  nativeCurrency: {
    decimals: 18,
    name: 'BNB',
    symbol: 'BNB',
  },
  rpcUrls: {
    default: { http: ['https://rpc.ankr.com/bsc'] },
    public: { http: ['https://rpc.ankr.com/bsc'] },
  },
  blockExplorers: {
    default: {
      name: 'BscScan',
      url: 'https://bscscan.com',
      apiUrl: 'https://api.bscscan.com/api',
    },
  },
  contracts: {
    multicall3: {
      address: '0xca11bde05977b3631167028862be2a173976ca11',
      blockCreated: 15921452,
    },
  },
};

const polygon = {
  id: 137,
  name: 'Polygon',
  nativeCurrency: { name: 'MATIC', symbol: 'MATIC', decimals: 18 },
  rpcUrls: {
    default: { http: ['https://polygon-rpc.com'] },
    public: { http: ['https://polygon-rpc.com'] },
  },
  blockExplorers: {
    default: {
      name: 'PolygonScan',
      url: 'https://polygonscan.com',
      apiUrl: 'https://api.polygonscan.com/api',
    },
  },
  contracts: {
    multicall3: {
      address: '0xca11bde05977b3631167028862be2a173976ca11',
      blockCreated: 25770160,
    },
  },
};

const mainnet = {
  id: 1,
  name: 'Ethereum',
  nativeCurrency: { name: 'Ether', symbol: 'ETH', decimals: 18 },
  rpcUrls: {
    default: {
      http: ['https://cloudflare-eth.com'],
    },
    public: {
      http: ['https://cloudflare-eth.com'],
    },
  },
  blockExplorers: {
    default: {
      name: 'Etherscan',
      url: 'https://etherscan.io',
      apiUrl: 'https://api.etherscan.io/api',
    },
  },
  contracts: {
    ensRegistry: {
      address: '0x00000000000C2E074eC69A0dFb2997BA6C7d2e1e',
    },
    ensUniversalResolver: {
      address: '0x8cab227b1162f03b8338331adaad7aadc83b895e',
      blockCreated: 18_958_930,
    },
    multicall3: {
      address: '0xca11bde05977b3631167028862be2a173976ca11',
      blockCreated: 14_353_601,
    },
  },
};

const avalanche = {
  id: 43_114,
  name: 'Avalanche',
  nativeCurrency: {
    decimals: 18,
    name: 'Avalanche',
    symbol: 'AVAX',
  },
  rpcUrls: {
    default: { http: ['https://api.avax.network/ext/bc/C/rpc'] },
    public: { http: ['https://api.avax.network/ext/bc/C/rpc'] },
  },
  blockExplorers: {
    default: {
      name: 'SnowScan',
      url: 'https://snowscan.xyz',
      apiUrl: 'https://api.snowscan.xyz/api',
    },
  },
  contracts: {
    multicall3: {
      address: '0xca11bde05977b3631167028862be2a173976ca11',
      blockCreated: 11907934,
    },
  },
};

const dogechain = {
  id: 2_000,
  name: 'Dogechain',
  nativeCurrency: {
    decimals: 18,
    name: 'Dogechain',
    symbol: 'DC',
  },
  rpcUrls: {
    default: { http: ['https://rpc.dogechain.dog'] },
    public: { http: ['https://rpc.dogechain.dog'] },
  },
  blockExplorers: {
    default: {
      name: 'DogeChainExplorer',
      url: 'https://explorer.dogechain.dog',
      apiUrl: 'https://explorer.dogechain.dog/api',
    },
  },
};

const wanchain = {
  id: 888,
  name: 'Wanchain',
  nativeCurrency: { name: 'WANCHAIN', symbol: 'WAN', decimals: 18 },
  rpcUrls: {
    default: {
      http: [
        'https://gwan-ssl.wandevs.org:56891',
        'https://gwan2-ssl.wandevs.org',
      ],
    },
    public: {
      http: [
        'https://gwan-ssl.wandevs.org:56891',
        'https://gwan2-ssl.wandevs.org',
      ],
    },
  },
  blockExplorers: {
    default: {
      name: 'WanScan',
      url: 'https://wanscan.org',
    },
  },
  contracts: {
    multicall3: {
      address: '0xcDF6A1566e78EB4594c86Fe73Fcdc82429e97fbB',
      blockCreated: 25312390,
    },
  },
};

const fantom = {
  id: 250,
  name: 'Fantom',
  nativeCurrency: {
    decimals: 18,
    name: 'Fantom',
    symbol: 'FTM',
  },
  rpcUrls: {
    default: { http: ['https://rpc.ankr.com/fantom'] },
    public: { http: ['https://rpc.ankr.com/fantom'] },
  },
  blockExplorers: {
    default: {
      name: 'FTMScan',
      url: 'https://ftmscan.com',
      apiUrl: 'https://api.ftmscan.com/api',
    },
  },
  contracts: {
    multicall3: {
      address: '0xca11bde05977b3631167028862be2a173976ca11',
      blockCreated: 33001987,
    },
  },
};

const cronos = {
  id: 25,
  name: 'Cronos Mainnet',
  nativeCurrency: {
    decimals: 18,
    name: 'Cronos',
    symbol: 'CRO',
  },
  rpcUrls: {
    default: { http: ['https://evm.cronos.org'] },
    public: { http: ['https://evm.cronos.org'] },
  },
  blockExplorers: {
    default: {
      name: 'Cronoscan',
      url: 'https://cronoscan.com',
      apiUrl: 'https://api.cronoscan.com/api',
    },
  },
  contracts: {
    multicall3: {
      address: '0xcA11bde05977b3631167028862bE2a173976CA11',
      blockCreated: 1963112,
    },
  },
};

const vechain = {
  id: 100009,
  name: 'Vechain',
  nativeCurrency: { name: 'VeChain', symbol: 'VET', decimals: 18 },
  rpcUrls: {
    default: { http: ['https://mainnet.vechain.org'] },
    public: { http: ['https://mainnet.vechain.org'] },
  },
  blockExplorers: {
    default: {
      name: 'Vechain Explorer',
      url: 'https://explore.vechain.org',
    },
    vechainStats: {
      name: 'Vechain Stats',
      url: 'https://vechainstats.com',
    },
  },
};

const classic = {
  id: 61,
  name: 'Ethereum Classic',
  nativeCurrency: {
    decimals: 18,
    name: 'ETC',
    symbol: 'ETC',
  },
  rpcUrls: {
    default: { http: ['https://etc.rivet.link'] },
    public: { http: ['https://etc.rivet.link'] },
  },
  blockExplorers: {
    default: {
      name: 'Blockscout',
      url: 'https://blockscout.com/etc/mainnet',
    },
  },
};

const phoenix = {
  id: 13381,
  name: 'Phoenix Blockchain',
  nativeCurrency: { name: 'Phoenix', symbol: 'PHX', decimals: 18 },
  rpcUrls: {
    default: { http: ['https://rpc.phoenixplorer.com'] },
    public: { http: ['https://rpc.phoenixplorer.com'] },
  },
  blockExplorers: {
    default: {
      name: 'Phoenixplorer',
      url: 'https://phoenixplorer.com',
      apiUrl: 'https://phoenixplorer.com/api',
    },
  },
  contracts: {
    multicall3: {
      address: '0x498cF757a575cFF2c2Ed9f532f56Efa797f86442',
      blockCreated: 5620192,
    },
  },
};

export default {
  bsc, polygon, mainnet, avalanche, dogechain, wanchain, fantom, cronos, vechain, classic, phoenix
};
