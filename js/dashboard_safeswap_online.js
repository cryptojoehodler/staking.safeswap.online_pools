const api_key = '10c64ff279-c84cead720-bf49ef52fb';
const cfg_pools = true;
const stakeCardTpl_old = '<div class="pb-3 h-100">    <div class="staking-card card-shadow  card h-100">        <input class="sousId" type="hidden" data-key="sousId">        <div class="card-header">            <div><div class="lbl-earn" data-key="lbl-earn">Earn ?????</div><div class="lbl-stake" data-key="lbl-stake">Stake MUXE</div></div>            <div class="icons">                <div class="icon-earn"><img data-key="icon-earn" src="gfx/sample_token.png" alt=""></div>                <div class="icon-stake"><img data-key="icon-stake" src="gfx/sample_token_tw.jpg" alt=""></div>            </div>        </div>        <div class="card-body">            <div class="card-body-section section-mode">                <table class="table table-borderless table-sm mb-0">                    <tr>                        <th>APR</th>                        <td data-key="apr">0.12%</td>                    </tr>                    <tr>                        <th>Deposit fee</th>                        <td data-key="deposit-fee">4%</td>                    </tr>                    <tr>                        <th>Transfer fee</th>                        <td data-key="transfer-fee">4%</td>                    </tr>                    <tr>                        <th>Withdrawal lockup</th>                        <td data-key="withdrawal-lockup">48 hours</td>                    </tr>                </table>            </div>            <div class="card-body-section section-actions harvest-section">                <div class="d-flex justify-content-between">                    <div class="earned-details w-100">                        <strong data-key="lbl-earned">????? earned</strong>                        <div class="earned-amount d-flex justify-content-between" data-key="earned-token">0.0000<span class="usd" style="float:right;margin-top:-25px;" data-key="earned-usd">(0 USD)</span></div>                    </div>                    <div style="width: 100px;">                        <button class="btn btn-outline-primary btn-sm btn-harvest btn-do-harvest">Harvest</button>                    </div>                </div>            </div>            <div d="stake-details" data-key="stake-details" class="card-body-section stake-details">                <div class="d-flex justify-content-between" style="align-items: center;">                    <div class="staked-details w-100">                        <strong data-key="lbl-staked">MUXE staked</strong>                        <div class="staked-amount d-flex justify-content-between" data-key="staked-token">0.0000<span class="usd" style="float:right;margin-top:-25px;" data-key="staked-usd">(0 USD)</span></div>                    </div>                    <div style="width: 100px;text-align: right;">                                                                        <div class="d-flex" style="gap: 5px;">                            <button data-key="btn-minus" data-contract="" class="btn btn-outline-primary btn-minus btn-do-unstake" style="border-radius: 20px; padding: 0; height: 40px; width: 40px;"><span class="fa-solid fa-minus"></span></button>                            <button data-key="btn-plus" data-contract="" class="btn btn-outline-primary btn-plus btn-do-stake" style="border-radius: 20px; padding: 0; height: 40px; width: 40px;"><span class="fa-solid fa-plus"></span></button>                        </div>                    </div>                </div>            </div>                        <button class="btn w-100 btn-connect mb-2"><i class="fa-solid fa-wallet"></i> Connect wallet</button>            <button id="btnEnable" data-key="btn-enable" data-contract="" data-spender_contract="" class="btn w-100 btn-primary btn-enable mb-2"><i class="fa-regular fa-square-check"></i> Enable</button>            <button class="btn btn-primary w-100 btn-swap mb-2"><i class="fa-solid fa-shuffle"></i> Swap</button>            <button class="btn btn-outline-primary w-100 btn-bottomsheet"><i class="fa-solid fa-magnifying-glass"></i> Details</button>                            <div class="card-body-section footer-details-graphically mt-2">                <div class="text-center footer-time">                    <i class="fa-solid fa-clock"></i> <span data-key="available-from">Jan. 1st 2024</span>                    <i class="fa-solid fa-arrow-right" style="margin: 0 10px;"></i>                    <i class="fa-solid fa-clock"></i> <span data-key="available-till">Jan. 14th 2024</span>                </div>                <div class="d-flex text-center footer-stats">                    <div>                        <div><strong>Min stake</strong></div>                        <div data-key="stake-min">0</div>                    </div>                    <div>                        <div><strong>Max stake</strong></div>                        <div id="stake-max-" data-key="stake-max">50,000,000</div>                    </div>                    <div>                        <div><strong>Can withdraw</strong></div>                        <div data-key="can-withdraw">Yes</div>                    </div>                    <div>                        <div><strong>Require hold</strong></div>                        <div data-key="require-hold">500 SWAP</div>                    </div>                </div>            </div>        </div>        <div class="bottom-sheet d-none staking-details">    <div class="bottom-sheet-content">        <div class="title">Staking details<div class="btn btn-sm btn-close-bottom-sheet"><i class="fa-solid fa-xmark"></i></div></div>        <table class="table">            <tr>                <th>Total staked</th>                <td>                    <span data-key="lbl-total-staked"> 0 MUXE</span><br/>                    <small data-key="total-staked-usd">(~ 2,586 USD)</small>                </td>            </tr>            <tr>                <th>Max. stake per user</th>                <td data-key="max-stake-per-user">                    5,000,000 MUXE                </td>            </tr>            <tr>                <th>Ends in</th>                <td data-key="ends-block">                    13,703,520 blocks                </td>            </tr>        </table>        <div class="links">            <a href="#" target="_blank" data-key="contract-details">Contract</a>            <a href="#">Project site</a>            <a href="#">Token info</a>            <a href="#" data-key="addMM" class="addMM" data-contract="" data-symbol="" data-decimals="" data-icon="">Add Metamask</a>        </div>    </div></div>        <div class="bottom-sheet d-none staking-stake">    <div class="bottom-sheet-content">        <div class="title">Stake<div class="btn btn-sm btn-close-bottom-sheet"><i class="fa-solid fa-xmark"></i></div></div>                <div class="d-flex justify-content-end mb-1">            <div class="mh-3">                <strong>Balance: </strong><span class="user-balance" data-key="user-balance">1344</span>            </div>        </div>        <div class="mb-3">            <div class="input-group">                <input type="number" class="form-control stake-amount" placeholder="0.0" aria-label="Stake amount" aria-describedby="basic-addon2" data-key="stake-amount" id="">                <span class="input-group-text" id="basic-addon2"><span data-key="stake-symbol">?????</span></span>                <span data-key="stake-usd" class="stake-usd">~ 0.1 USD</span>            </div>        </div>                <div class="d-flex justify-content-between mb-1">            <div class="mh-3">                <strong>Min: </strong><span data-key="stake-min">0</span>            </div>            <div class="mh-3">                <strong>Max: </strong><span data-key="stake-max">0</span>            </div>        </div>                        <div class="progress mb-3">            <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>        </div>        <div class="d-flex justify-content-center mb-3" style="gap: 10px;">            <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="25">25%</button>            <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="50">50%</button>            <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="75">75%</button>            <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="100">Max</button>        </div>        <div class="d-flex" style="gap: 10px;">            <button class="btn btn-primary w-100 btnStake" data-contract="" data-decimals="" data-symbol="" data-spender="" data-key="btnStakingConfirm">Confirm</button>            <button class="btn btn-outline-secondary w-100">Get <span data-key="stake-symbol">?????</span></button>        </div>    </div></div>        <div class="bottom-sheet d-none staking-unstake">    <div class="bottom-sheet-content">        <div class="title">Unstake<div class="btn btn-sm btn-close-bottom-sheet"><i class="fa-solid fa-xmark"></i></div></div>                <div class="d-flex justify-content-end mb-1">            <div class="mh-3">                <strong>Currently staked: </strong><span class="stake-balance" data-key="stake-balance">1344</span>            </div>        </div>                <div class="mb-3">            <div class="input-group"> <input type="number" class="form-control unstake-amount" placeholder="0.0" aria-label="Stake amount" aria-describedby="basic-addon2" data-key="unstake-amount" id="">                <span class="input-group-text" id="basic-addon2" data-key="stake-amount">                    <span data-key="stake-symbol">?????</span>                </span>                <span data-key="stake-usd" class="stake-usd">~ 0.1 USD</span>            </div>        </div>                        <div class="progress mb-3">            <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>        </div>        <div class="d-flex justify-content-center mb-3" style="gap: 10px;">            <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="25">25%</button>            <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="50">50%</button>            <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="75">75%</button>            <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="100">Max</button>        </div>        <div class="d-flex" style="gap: 10px;">            <button class="btn btn-primary w-100 btnUnStake" data-contract="" data-decimals="" data-symbol="" data-sousId="" data-key="btnUnStakingConfirm">Confirm</button>        </div>    </div></div>        <div class="bottom-sheet d-none staking-harvest">    <div class="bottom-sheet-content">        <div class="title">Harvest<div class="btn btn-sm btn-close-bottom-sheet"><i class="fa-solid fa-xmark"></i></div></div>                <div class="mb-3">            <div class="input-group">                <div data-key="harvest-amount" class="harvest-amount">123</div>                                <span class="input-group-text" id="basic-addon2"><span data-key="harvest-symbol">?????</span></span>                <span data-key="harvest-usd" class="stake-usd">~ 0.1 USD</span>            </div>        </div>        <div class="d-flex" style="gap: 10px;">            <button class="btn btn-primary w-100 btnHarvest" data-contract="" data-symbol="" data-key="btn-harvest-confirm">Confirm</button>        </div>    </div></div>    </div></div>';var wallet_address = null;

const stakeCardTpl = `
    <div class="pb-3 h-100">
        <div class="staking-card card-shadow card h-100">
            <input class="sousId" type="hidden" data-key="sousId">
            <div class="card-header">
                <div>
                    <div class="lbl-earn" data-key="lbl-earn">Earn ?????</div>
                    <div class="lbl-stake" data-key="lbl-stake">Stake MUXE</div>
                </div>
                <div class="icons">
                    <div class="icon-earn">
                        <img data-key="icon-earn" src="gfx/sample_token.png" alt="">
                    </div>
                    <div class="icon-stake"><img data-key="icon-stake" src="gfx/sample_token_tw.jpg" alt=""></div>
                </div>
            </div>
            <div class="card-body">
                <div class="card-body-section section-mode">
                    <table class="table table-borderless table-sm mb-0">
                        <tr>
                            <th>APR</th>
                            <td data-key="apr">0.12%</td>
                        </tr>
                        <tr>
                            <th>Deposit fee</th>
                            <td data-key="deposit-fee">4%</td>
                        </tr>
                        <tr>
                            <th>Transfer fee</th>
                            <td data-key="transfer-fee">4%</td>
                        </tr>
                        <tr>
                            <th>Withdrawal lockup</th>
                            <td data-key="withdrawal-lockup">48 hours</td>
                        </tr>
                    </table>
                </div>
                <div class="card-body-section section-actions harvest-section">
                    <div class="d-flex justify-content-between">
                        <div class="earned-details w-100">
                            <strong data-key="lbl-earned">????? earned</strong>                        
                            <div 
                                class="earned-amount d-flex justify-content-between" 
                                data-key="earned-token"
                            >
                                0.0000<span class="usd" style="float: right; margin-top: -25px;" data-key="earned-usd">(0 USD)</span>
                            </div>
                        </div>
                        <div style="width: 100px;">
                            <button class="btn btn-outline-primary btn-sm btn-harvest btn-do-harvest">Harvest</button>
                        </div>
                    </div>
                </div>
                <div id="stake-details" data-key="stake-details" class="card-body-section stake-details">
                    <div class="d-flex justify-content-between" style="align-items: center;">
                        <div class="staked-details w-100">
                            <strong data-key="lbl-staked">MUXE staked</strong>                        
                            <div class="staked-amount d-flex justify-content-between" data-key="staked-token">
                                0.0000<span class="usd" style="float: right; margin-top: -25px;" data-key="staked-usd">(0 USD)</span>
                            </div>
                        </div>
                        <div style="width: 100px; text-align: right;">
                            <div class="d-flex" style="gap: 5px;">
                                <button 
                                    data-key="btn-minus" 
                                    data-contract="" 
                                    class="btn btn-outline-primary btn-minus btn-do-unstake" 
                                    style="border-radius: 20px; padding: 0; height: 40px; width: 40px;"
                                >
                                    <span class="fa-solid fa-minus"></span>
                                </button>
                                <button 
                                    data-key="btn-plus" 
                                    data-contract="" 
                                    class="btn btn-outline-primary btn-plus btn-do-stake" 
                                    style="border-radius: 20px; padding: 0; height: 40px; width: 40px;"
                                >
                                    <span class="fa-solid fa-plus"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn w-100 btn-connect mb-2">
                    <i class="fa-solid fa-wallet"></i> Connect wallet
                </button>
                <button id="btnEnable" data-key="btn-enable" data-contract="" data-spender_contract="" class="btn w-100 btn-primary btn-enable mb-2">
                    <i class="fa-regular fa-square-check"></i> Enable
                </button>
                <button class="btn btn-primary w-100 btn-swap mb-2">
                    <i class="fa-solid fa-shuffle"></i> Swap
                </button>
                <button class="btn btn-outline-primary w-100 btn-bottomsheet">
                    <i class="fa-solid fa-magnifying-glass"></i> Details
                </button>                            
                <div class="card-body-section footer-details-graphically mt-2">
                    <div class="text-center footer-time">
                        <i class="fa-solid fa-clock"></i>
                        <span data-key="available-from">Jan. 1st 2024</span>
                        <i class="fa-solid fa-arrow-right" style="margin: 0 10px;"></i>
                        <i class="fa-solid fa-clock"></i>
                        <span data-key="available-till">Jan. 14th 2024</span>
                    </div>
                    <div class="d-flex text-center footer-stats">
                        <div>
                            <div><strong>Min stake</strong></div>
                            <div data-key="stake-min">0</div>
                        </div>
                        <div>
                            <div><strong>Max stake</strong></div>
                            <div id="stake-max-" data-key="stake-max">50, 000, 000</div>
                        </div>
                        <div>
                            <div><strong>Can withdraw</strong></div>
                            <div data-key="can-withdraw">Yes</div>
                        </div>
                        <div>
                            <div><strong>Require hold</strong></div>
                            <div data-key="require-hold">500 SWAP</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-sheet d-none staking-details">
                <div class="bottom-sheet-content">
                    <div class="title">
                        Staking details
                        <div class="btn btn-sm btn-close-bottom-sheet">
                            <i class="fa-solid fa-xmark"></i>
                        </div>
                    </div>
                    <table class="table">
                        <tr>
                            <th>Total staked</th>
                            <td>
                                <span data-key="lbl-total-staked">0 MUXE</span><br/><small data-key="total-staked-usd">(~2,586 USD)</small>
                            </td>
                        </tr>
                        <tr>
                            <th>Max. stake per user</th>
                            <td data-key="max-stake-per-user">5,000,000 MUXE</td>
                        </tr>
                        <tr>
                            <th>Ends in</th>
                            <td data-key="ends-block">13,703,520 blocks</td>
                        </tr>
                    </table>
                    <div class="links">
                        <a href="#" target="_blank" data-key="contract-details">Contract</a>
                        <a href="#">Project site</a>
                        <a href="#">Token info</a>
                        <a href="#" data-key="addMM" class="addMM" data-contract="" data-symbol="" data-decimals="" data-icon="">Add Metamask</a>
                    </div>
                </div>
            </div>
            <div class="bottom-sheet d-none staking-stake">
                <div class="bottom-sheet-content">
                    <div class="title">
                        Stake - <span data-key="stake-symbol"></span>
                        <div class="btn btn-sm btn-close-bottom-sheet">
                            <i class="fa-solid fa-xmark"></i>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end mb-1">
                        <div class="mh-3">
                            <strong>Balance: </strong><span class="user-balance" data-key="user-balance">1344</span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <input type="number" class="form-control stake-amount" placeholder="0.0" aria-label="Stake amount" aria-describedby="basic-addon2" data-key="stake-amount" id="">
                    </div>
                    <div class="d-flex justify-content-between mb-1">
                        <div class="mh-3">
                            <strong>Min: </strong><span data-key="stake-min">0</span>
                        </div>
                        <div class="mh-3">
                            <strong>Max: </strong><span data-key="stake-max">0</span>
                        </div>
                    </div>
                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="d-flex justify-content-center mb-3" style="gap: 10px;">
                        <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="25">25%</button>
                        <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="50">50%</button>
                        <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="75">75%</button>
                        <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="100">Max</button>
                    </div>
                    <div class="d-flex" style="gap: 10px;">
                        <button class="btn btn-primary w-100 btnStake" data-contract="" data-decimals="" data-symbol="" data-spender="" data-key="btnStakingConfirm">Confirm</button>
                        <button class="btn btn-outline-secondary w-100">
                            Get <span data-key="stake-symbol">?????</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="bottom-sheet d-none staking-unstake">
                <div class="bottom-sheet-content">
                    <div class="title">
                        Unstake - <span data-key="stake-symbol"></span>
                        <div class="btn btn-sm btn-close-bottom-sheet">
                            <i class="fa-solid fa-xmark"></i>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end mb-1">
                        <div class="mh-3">
                            <strong>Currently staked: </strong><span class="stake-balance" data-key="stake-balance">1344</span>
                        </div>
                    </div>
                    <div class="mb-3">
                        <input 
                            type="number" 
                            class="form-control unstake-amount" 
                            placeholder="0.0" 
                            aria-label="Stake amount" 
                            aria-describedby="basic-addon2" 
                            data-key="unstake-amount" 
                            id=""
                            style="text-overflow: ellipsis;"
                        >
                    </div>
                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="d-flex justify-content-center mb-3" style="gap: 10px;">
                        <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="25">25%</button>
                        <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="50">50%</button>
                        <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="75">75%</button>
                        <button class="btn btn-sm btn-outline-primary progress-step w-100" data-percent="100">Max</button>
                    </div>
                    <div class="d-flex" style="gap: 10px;">
                        <button class="btn btn-primary w-100 btnUnStake" data-contract="" data-decimals="" data-symbol="" data-sousId="" data-key="btnUnStakingConfirm">Confirm</button>
                    </div>
                </div>
            </div>
            <div class="bottom-sheet d-none staking-harvest">
                <div class="bottom-sheet-content">
                    <div class="title">
                        Harvest - <span data-key="harvest-symbol"></span>
                        <div class="btn btn-sm btn-close-bottom-sheet">
                            <i class="fa-solid fa-xmark"></i>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div data-key="harvest-amount" class="harvest-amount">123</div>          
                    </div>
                    <div class="d-flex" style="gap: 10px;">
                        <button class="btn btn-primary w-100 btnHarvest" data-contract="" data-symbol="" data-key="btn-harvest-confirm">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
`;

// Required by some npm packages
window.process = { browser: true, env: { ENVIRONMENT: 'BROWSER' }, };

const CoreModal = {
    // Variables
    core_modal_id: 'core-modal',
    element: null,
    modal: null,

    _init: function () {
        // Check if modal doesnt exist with the class
        if($('#' + this.core_modal_id).length === 0) {
            // Create bootstrap modal
            this.element = $('<div class="modal fade" id="' + this.core_modal_id + '" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"></div>');
            this.element.append('<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable"></div>');
            this.element.find('.modal-dialog').append('<div class="modal-content"></div>');

            // Append modal to body
            $('body').append(this.element);

            this.element.modal();
        }
        return this;
    },

    el: function() {
        return this.element;
    },

    small: function() {
        this.element.find('.modal-dialog').addClass('modal-sm');
        return this;
    },

    nopadding: function() {
        this.element.find('.modal-body').addClass('p-0');
        return this;
    },

    large: function() {
        this.element.find('.modal-dialog').addClass('modal-lg');
        return this;
    },

    extraLarge: function() {
        this.element.find('.modal-dialog').addClass('modal-xl');
        return this;
    },

    make: function(title, body, buttons) {
        this._init();

        this._header(title);
        this._content(body);
        this._footer(buttons);

        return this;
    },

    show: function(title, body, buttons) {
        this.element.modal('show');
        return this;
    },

    hide: function() {
        this.element.modal('hide');
        return this;
    },

    _header: function(title) {
        // Check if header exists
        if(this.element.find('.modal-header').length == 0) {
            this.element.find('.modal-content').append('<div class="modal-header"></div>');
        }

        this.element.find('.modal-header').html('<strong>' + title + '</strong>');
        // Add close button
        this.element.find('.modal-header').append('<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>');
    },

    _content: function(content) {
        // Check if content exists
        if(this.element.find('.modal-body').length == 0) {
            this.element.find('.modal-content').append('<div class="modal-body"></div>');
        }
        
        this.element.find('.modal-body').html(content);
    },

    _footer: function(buttons) {
        // Check if footer exists
        if(this.element.find('.modal-footer').length == 0) {
            this.element.find('.modal-content').append('<div class="modal-footer"></div>');
        }

        // Clear footer
        this.element.find('.modal-footer').html('');

        // Check if buttons is an array
        if(Array.isArray(buttons)) {
            // Loop through buttons
            for (let i = 0; i < buttons.length; i++) {
                var buttonEl = null;
                // Check if button is an object
                if(typeof buttons[i] === 'object') {
                    let classes = 'btn-primary';
                    let id = '';

                    // Check if button has an id
                    if(typeof buttons[i].id !== 'undefined') {
                        id = ' id="' + buttons[i].id + '"';
                    }

                    // Check if button has a class
                    if(typeof buttons[i].class !== 'undefined') {
                        classes = buttons[i].class;
                    }

                    // Check if button has a class
                    buttonEl = $('<button type="button" class="btn ' + classes + '"' + id + '>' + buttons[i].text + '</button>');
                }else{
                    buttonEl = $('<button type="button" class="btn btn-primary">' + buttons[i] + '</button>');
                }
                this.element.find('.modal-footer').append(buttonEl);

                // Check if button has a callback
                if(typeof buttons[i].callback === 'function') {
                    buttonEl.click(function() {
                        buttons[i].callback(this);
                    });
                }
            }
        }
    }
};

import wagmiChains from './chains.js';
import { ethers } from "./ethers.js";
import {
    EthereumClient,
    w3mConnectors,
    w3mProvider,
    WagmiCore,
    WagmiCoreConnectors,
  } from "https://unpkg.com/@web3modal/ethereum";
  
import { Web3Modal } from "https://unpkg.com/@web3modal/html";
  
const projectId = "c7edafd834ddb18f7490d0a984c0812b";

// 0. Import wagmi dependencies
const { fetchBlockNumber, useNetwork, useSwitchNetwork, configureChains, createConfig, switchNetwork, readContract, writeContract, getAccount, watchAccount, disconnect, readContracts, waitForTransaction, useBlockNumber } = WagmiCore;

// 1. Define chains
// As chains are added to chains.js, add the object name to the chains array.
const chains = [
    wagmiChains.bsc,
    wagmiChains.polygon,
    wagmiChains.mainnet,
    wagmiChains.avalanche,
    wagmiChains.dogechain,
    wagmiChains.wanchain,
    wagmiChains.fantom,
    wagmiChains.cronos,
    wagmiChains.classic,
    wagmiChains.vechain,
    wagmiChains.phoenix
];

// 2. Configure wagmi client
const { publicClient } = configureChains(chains, [w3mProvider({ projectId })]);
const wagmiConfig = createConfig({
  autoConnect: true,
  connectors: [
    ...w3mConnectors({ chains, version: 2, projectId }),
    new WagmiCoreConnectors.CoinbaseWalletConnector({
      chains,
      options: {
        appName: "SafeSwap Staking Pool",
      },
    }),
  ],
  publicClient,
});

if($('.filter-bar-wrapper').length > 0) {
    const filter_bar_html = `
        <div class="d-none d-lg-block d-lg-flex filter-bar mb-4 row">
            <div class="col-sm-12 col-md-6 col-lg-9">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-5 mb-3">
                        <div class="btn-group w-100">
                            <a href="#" id="btnShowStaked" class="btn btn-outline-primary" aria-current="page">Show staked only</a>
                            <a href="#" id="btnShowAll" class="btn btn-outline-primary active">Show all</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-5 mb-3">
                        <div class="btn-group w-100">
                            <a href="#" id="btnLive" class="btn btn-outline-primary active" aria-current="page">Live</a>
                            <a href="#" id="btnFinished" class="btn btn-outline-primary ">Finished</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-2 mb-3">
                        <div class="dropdown w-100">
                            <button class="btn btn-outline-primary dropdown-toggle w-100" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Sort by: Hot
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item pool-sort active" href="#">Hot</a></li>
                                <li><a class="dropdown-item pool-sort" data-sort="apr" href="#">APR</a></li>
                                <li><a class="dropdown-item pool-sort" data-sort="earned" href="#">Earned</a></li>
                                <li><a class="dropdown-item pool-sort" data-sort="staked" href="#">Total staked</a></li>
                                <li><a class="dropdown-item pool-sort" data-sort="name" href="#">Name</a></li>
                                <li><a class="dropdown-item pool-sort" data-sort="end-date" href="#">End date</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 d-flex mb-3" style="gap: 20px;">
                <div style="flex:1;">
                    <input class="form-control me-2" type="search" id="staking-search" placeholder="Search" aria-label="Search">
                </div>
                <div>
                    <button class="btn btn-outline-primary btn-staking-reload">
                        <i class="fa-solid fa-rotate-right"></i>
                    </button>
                </div>
            </div>
        </div>
    `;

    $('.filter-bar-wrapper').append(filter_bar_html);
}

var sausIdToAddress = {};

// 3. Create ethereum and modal clients
const ethereumClient = new EthereumClient(wagmiConfig, chains);

export const web3Modal = new Web3Modal({
    projectId,
    chainImages: {
        2000: "/gfx/chain_logos/dogechain.jpeg",
        888: "/gfx/chain_logos/wanchain-logo.png",
        250: "/gfx/chain_logos/Fantom_round.png",
        25: "/gfx/chain_logos/cro_token_logo.png",
        100009: "/gfx/chain_logos/VET_Token_Icon.png",
        61: "/gfx/chain_logos/ethereum-classic-logo.png",
        13381: "/gfx/chain_logos/phoenix.png"
    }
  },
  ethereumClient
);

web3Modal.setDefaultChain(wagmiChains.bsc);

let selectedChain = {};
let currentNetwork = {};

if(ethereumClient.getNetwork().chain == undefined) {
    selectedChain = {
        chainId: wagmiChains.bsc.id
    }

    currentNetwork.id = wagmiChains.bsc.id;
} else {
    selectedChain = {
        chainId: ethereumClient.getNetwork().chain.id
    }

    currentNetwork.id = ethereumClient.getNetwork().chain.id;
}

setInterval(() => {
    if(ethereumClient.getNetwork().chain != undefined && currentNetwork.id != ethereumClient.getNetwork().chain.id) {
        currentNetwork = ethereumClient.getNetwork().chain;

        if(currentNetwork) {
            ethereumClient.switchNetwork({
                chainId: currentNetwork.id 
            });

            selectedChain = {
                chainId: currentNetwork.id
            }

            safeswap.fetchSafeSwapPools();
        }
    }
}, 200);

const ABI = JSON.parse('[{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"tokenRecovered","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"AdminTokenRecovery","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"user","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"user","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"EmergencyWithdraw","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"depositFee","type":"uint256"}],"name":"NewDepositFee","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"maxAmount","type":"uint256"}],"name":"NewMaxStakedAmount","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"maxAmount","type":"uint256"}],"name":"NewMinDepositAmount","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"maxAmount","type":"uint256"}],"name":"NewMinHoldTokenBalance","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"poolLimitPerUser","type":"uint256"}],"name":"NewPoolLimit","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"rewardPerBlock","type":"uint256"}],"name":"NewRewardPerBlock","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint16","name":"transferFee","type":"uint16"}],"name":"NewStakedTokenTransferFee","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"startBlock","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"endBlock","type":"uint256"}],"name":"NewStartAndEndBlocks","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"interval","type":"uint256"}],"name":"NewWithdrawalInterval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"blockNumber","type":"uint256"}],"name":"RewardsStop","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"whiteListedAddress","type":"address"},{"indexed":false,"internalType":"bool","name":"whitelisted","type":"bool"}],"name":"WhiteListAddress","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"user","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Withdraw","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"WithdrawFees","type":"event"},{"inputs":[],"name":"MAXIMUM_WITHDRAWAL_INTERVAL","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"PRECISION_FACTOR","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"SWAP_BOX_FACTORY","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"accTokenPerShare","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"bonusEndBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_user","type":"address"}],"name":"canWithdraw","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"deposit","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"depositFee","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"emergencyRewardWithdraw","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"emergencyWithdraw","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"user","type":"address"},{"internalType":"bool","name":"exclude","type":"bool"}],"name":"excludeFromFees","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"fees","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"hasHoldTokenLimit","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"hasMaxStakedLimit","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"hasMinDepositLimit","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"hasUserLimit","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"holdToken","outputs":[{"internalType":"contract IBEP20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"contract IBEP20","name":"_stakedToken","type":"address"},{"internalType":"contract IBEP20","name":"_rewardToken","type":"address"},{"internalType":"uint256","name":"_rewardPerBlock","type":"uint256"},{"internalType":"uint256","name":"_startBlock","type":"uint256"},{"internalType":"uint256","name":"_bonusEndBlock","type":"uint256"},{"internalType":"uint256","name":"_poolLimitPerUser","type":"uint256"},{"internalType":"uint16","name":"_stakedTokenTransferFee","type":"uint16"},{"internalType":"uint256","name":"_depositFee","type":"uint256"},{"internalType":"uint256","name":"_withdrawalInterval","type":"uint256"},{"internalType":"address","name":"_admin","type":"address"}],"name":"initialize","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"}],"name":"isExcludedFromDepositFee","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"isInitialized","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"lastRewardBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"maxStakedAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"minAmountToHold","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"minDepositAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_user","type":"address"}],"name":"pendingReward","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"poolLimitPerUser","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_tokenAddress","type":"address"},{"internalType":"uint256","name":"_tokenAmount","type":"uint256"}],"name":"recoverWrongTokens","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"rewardPerBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"rewardToken","outputs":[{"internalType":"contract IBEP20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"stakedToken","outputs":[{"internalType":"contract IBEP20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"stakedTokenTransferFee","outputs":[{"internalType":"uint16","name":"","type":"uint16"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"startBlock","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"stopReward","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"totalStaked","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_depositFee","type":"uint256"}],"name":"updateDepositFee","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bool","name":"_hasMaxStakedLimit","type":"bool"},{"internalType":"uint256","name":"_maxStakedAmount","type":"uint256"}],"name":"updateMaxStakedAmount","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bool","name":"_hasMinDepositLimit","type":"bool"},{"internalType":"uint256","name":"_minDepositAmount","type":"uint256"}],"name":"updateMinDepositAmount","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"contract IBEP20","name":"_holdToken","type":"address"},{"internalType":"bool","name":"_hasHoldTokenLimit","type":"bool"},{"internalType":"uint256","name":"_minAmountToHold","type":"uint256"}],"name":"updateMinHoldTokenAmount","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bool","name":"_hasUserLimit","type":"bool"},{"internalType":"uint256","name":"_poolLimitPerUser","type":"uint256"}],"name":"updatePoolLimitPerUser","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_rewardPerBlock","type":"uint256"}],"name":"updateRewardPerBlock","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint16","name":"_transferFee","type":"uint16"}],"name":"updateStakedTokenTransferFee","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_startBlock","type":"uint256"},{"internalType":"uint256","name":"_bonusEndBlock","type":"uint256"}],"name":"updateStartAndEndBlocks","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_interval","type":"uint256"}],"name":"updateWithdrawalInterval","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"userInfo","outputs":[{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"uint256","name":"rewardDebt","type":"uint256"},{"internalType":"uint256","name":"nextWithdrawalUntil","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"withdraw","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"withdrawFees","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"withdrawalInterval","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}]');
const stakingABI = JSON.parse('[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"spender","type":"address"},{"name":"tokens","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"from","type":"address"},{"name":"to","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"_totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"tokens","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"tokenOwner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"to","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"tokensBurnt","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"spender","type":"address"},{"name":"tokens","type":"uint256"},{"name":"data","type":"bytes"}],"name":"approveAndCall","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"tokenAddress","type":"address"},{"name":"tokens","type":"uint256"}],"name":"transferAnyERC20Token","outputs":[{"name":"success","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"tokenOwner","type":"address"},{"name":"spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"payable":true,"type":"fallback"},{"anonymous":false,"inputs":[{"indexed":false,"name":"tokens","type":"uint256"}],"name":"Burn","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"tokens","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"tokenOwner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"tokens","type":"uint256"}],"name":"Approval","type":"event"}]');
var tokenPrices = [];
const delay = ms => new Promise(res => setTimeout(res, ms));
const approveAmount = '115792089237316195423570985008687907853269984665640564039457584007913129639935';
var showStaked = false;
var showAll = true;
var showStakedOnly = false;
var showStake = true;
var showLivePools = true;
var explorerURL = 'https://bscscan.com/';

const safeswap = {
    test_mode: false,
    ssadm_url: 'https://dashboard.safeswap.online',

    dateStamp: new Date().toISOString().substr(0, 16).replace('T', ' ').replaceAll('-', '').replaceAll(':', '').replaceAll(' ', ''),

    fetchTokenPrices: async function() {
        debug('SafeSwap: fetchTokenPrices()');
        // let api_url = '/json/token_prices.json';
        // let api_url = 'https://api.safeswap.finance/api/tokens';
        let api_url = 'https://dashboard.safeswap.online/api/tokenprice';

        if (this.test_mode) {
            api_url = '/json/token_prices.json'
        }

        await fetch(api_url).then(response => response.json()).then(data => {
            debug('[fetchTokenPrices] done', data);

            $.each(data, function(token_address, price) {
                tokenPrices[token_address] = price;
            });
        });
    },

    fetchSafeSwapPools: async function (callback = () => {}) {
        await safeswap.fetchTokenPrices();

        if(typeof cfg_pools == 'undefined' && typeof cfg_vesting == 'undefined' && typeof cfg_allpools == 'undefined') {
            return false;
        }

        let api_url = '';

        if(cfg_pools) {
            api_url = `${this.ssadm_url}/api/pools/?f=&v=${this.dateStamp}&chain_id=${selectedChain.chainId}&api_key=${api_key}`;
            if (this.test_mode) {
                api_url = '/json/pools.json'
            }
        } else if(cfg_vesting) {
            api_url = `${this.ssadm_url}/api/vesting/pools/?f=&v=${this.dateStamp}&w=${wallet_address}&chain_id=${selectedChain.chainId}&api_key=${api_key}`;
            if (this.test_mode) {
                api_url = '/json/pools_vesting.json'
            }
        } else if(cfg_allpools) {
            api_url = `${this.ssadm_url}/api/allpools/?f=&v=${this.dateStamp}&w=${wallet_address}&chain_id=${selectedChain.chainId}&api_key=${api_key}`;
            if (this.test_mode) {
                api_url = '/json/pools_all.json'
            }
        }

        let currentBlock = await fetchBlockNumber();
            currentBlock = Number(currentBlock);

        fetch(api_url).then(response => response.json()).then(data => {
            let filteredPools = [];

            if(data) {
                if(showLivePools) {
                    filteredPools = data.filter(pool => pool.isFinished == false);
                } else {
                    filteredPools = data.filter(pool => pool.isFinished == true);
                }
            }

            $('.staking-cards').html('');

            if(filteredPools.length == 0) {
                $('.staking-cards').html('<div class="no-pools-found cards-loading text-center p-5">No pools found...</div>');
            } else {
                filteredPools.forEach(pool => {
                    this.displayPool(currentBlock, pool);
                });
            }

            callback();
        });
    },

    displayPool: async function (currentBlock, pool) {
        const date_start = pool.date_start ? new Date(pool.date_start) : null;
        const date_end = pool.date_end ? new Date(pool.date_start) : null;

        let stakingToken = pool.stakingToken;
        let earningToken = pool.earningToken;
        let isFinished = pool.isFinished;
        let sousId = pool.sousId;
        let tokenPerBlock = pool.tokenPerBlock;
        let harvest = pool.harvest;
        let safeVault = pool.safeVault;
        let sortOrder = pool.sortOrder;
        let stakingTokenDecimals = pool.stakingToken.decimals;
        let earningTokenDecimals = pool.earningToken.decimals;
        let contractAddress = pool.contractAddress[selectedChain.chainId];

        let balanceOf = 0;
        let allowance = 0;

        sausIdToAddress[sousId] = {
            'staking': stakingToken.address[selectedChain.chainId],
            'earning': earningToken.address[selectedChain.chainId]
        };

        let iconStaking = `https://safeswap.online/images/tokens/${stakingToken.address[selectedChain.chainId]}.png`;
        let iconEarning = `https://safeswap.online/images/tokens/${earningToken.address[selectedChain.chainId]}.png`;

        if (stakingToken.icon != null) {
            iconStaking = stakingToken.icon
        }

        if (earningToken.icon != null) {
            iconEarning = earningToken.icon
        }

        const stakingTokenPrice = tokenPrices[stakingToken.address[selectedChain.chainId]];
        const earningTokenPrice = tokenPrices[earningToken.address[selectedChain.chainId]];

        const staking_Contract = {
            address: stakingToken.address[selectedChain.chainId],
            abi: stakingABI,
        }

        const stakingContract = {
            address: contractAddress,
            abi: ABI,
        }

        if(wallet_address != null) {
            const contract_data = await readContracts({
                contracts: [
                    {
                        ...staking_Contract,
                        functionName: 'allowance',
                        args: [wallet_address, contractAddress]
                    },
                    {
                        ...staking_Contract,
                        functionName: 'balanceOf',
                        args: [wallet_address]
                    }
                ]
            });

            allowance = ethers.utils.formatUnits(contract_data[0].result, stakingTokenDecimals);
            balanceOf = ethers.utils.formatUnits(contract_data[1].result, stakingTokenDecimals);
        }

        const data = await readContracts({
            contracts: [
                {
                    ...stakingContract,
                    functionName: 'depositFee',
                },
                {
                    ...stakingContract,
                    functionName: 'stakedTokenTransferFee',
                },
                {
                    ...stakingContract,
                    functionName: 'withdrawalInterval',
                },
                {
                    ...stakingContract,
                    functionName: 'pendingReward',
                    args: [wallet_address],
                },
                {
                    ...stakingContract,
                    functionName: 'userInfo',
                    args: [wallet_address],
                },
                {
                    ...stakingContract,
                    functionName: 'minDepositAmount',
                },
                {
                    ...stakingContract,
                    functionName: 'poolLimitPerUser',
                },
                {
                    ...stakingContract,
                    functionName: 'canWithdraw',
                    args: [wallet_address],
                },
                {
                    ...stakingContract,
                    functionName: 'totalStaked',
                },
                {
                    ...stakingContract,
                    functionName: 'bonusEndBlock',
                }
            ]
        });

        const depositFee = Number(data[0].result) / 100;
        const transferFee = Number(data[1].result) / 100;
        const withdrawalInterval = Number(data[2].result) / 3600;

        let pendingReward = 0;
        let pendingRewardReadable = 0;
        let stakedToken = 0;
        let stakedTokenReadable = 0;

        if(wallet_address != null) {
            stakedToken = ethers.utils.formatUnits(data[4].result[0], stakingTokenDecimals);
            stakedTokenReadable = Math.floor(stakedToken * 1000) / 1000;

            pendingReward = ethers.utils.formatUnits(data[3].result, earningTokenDecimals);
            pendingRewardReadable = Math.floor(pendingReward * 10000000) / 10000000;
        }

        if(wallet_address != null && stakedToken == 0 && showStaked == true) {
            return false;
        }



        const minDepositAmount = data[5].result == undefined ? 0 : Math.floor(ethers.utils.formatUnits(data[5].result, stakingTokenDecimals));
        const maxStakedAmount = data[6].result == undefined ? 0 : Math.floor(ethers.utils.formatUnits(data[6].result, stakingTokenDecimals));
        const canWithdraw = data[7].result;
        const totalStaked = ethers.utils.formatUnits(data[8].result, stakingTokenDecimals);
        const bonusEndBlock = Number(data[9].result);

        if(showLivePools && (currentBlock > bonusEndBlock)) {
            return false;
        }

        const endBlock = (bonusEndBlock - currentBlock) > 0 ? (bonusEndBlock - currentBlock) : 0;
        const canWithdrawText = canWithdraw == true ? "Yes" : "No";
        const maxStakeAmountText = maxStakedAmount == 0 ? "No Limit" : maxStakedAmount;

        let earningUSD = pendingReward * earningTokenPrice;
        if(isNaN(earningUSD)) { 
            earningUSD = 0; 
        }

        let stakingUSD = stakedToken * stakingTokenPrice;
        if(isNaN(stakingUSD)) { 
            stakingUSD = 0; 
        }

        const totalStakedUSD = totalStaked * stakingTokenPrice;

        let templateHtml = '';
        let template = stakeCardTpl;

        if(showLivePools) {
            if($('.staking-cards').find('[data-poolid="' + sousId + '"]').length == 0) {
                templateHtml = $('<div class="col-sm-12 col-md-6 col-lg-4" data-poolid="' + sousId + '">' + template + '</div>');
                $('.staking-cards').append(templateHtml);
            } else {
                templateHtml = $('.staking-cards').find('[data-poolid="' + sousId + '"]');
            }
        } else {
            templateHtml = $('<div class="col-sm-12 col-md-6 col-lg-4" data-poolid="' + sousId + '">' + template + '</div>');

            if(showStaked == true && stakedToken > 0) {
                let el = $('.staking-cards').append(templateHtml);
            } else if(showAll == true) {
                let el = $('.staking-cards').append(templateHtml);
            }
        }

        let apr = calcPoolApr(stakingTokenPrice, earningTokenPrice, totalStaked, tokenPerBlock);

        if (isNaN(apr) || apr == null) { 
            apr = 0; 
        }

        templateHtml.find('[data-key="apr"]').html(apr.toFixed(2) + '%');
        templateHtml.find('[data-key="sousId"]').attr('data-id', sousId);
        templateHtml.find('[data-key="sousId"]').attr('data-contract', contractAddress);
        templateHtml.find('[data-key="sousId"]').attr('data-stakingdecimals', stakingToken.decimals);
        templateHtml.find('[data-key="sousId"]').attr('data-earningdecimals', earningToken.decimals);
        templateHtml.find('[data-key="lbl-earn"]').html('Earn ' + earningToken.symbol);
        templateHtml.find('[data-key="lbl-stake"]').html('Stake ' + stakingToken.symbol);
        templateHtml.find('[data-key="icon-earn"]').attr('src', iconEarning);
        templateHtml.find('[data-key="icon-stake"]').attr('src', iconStaking);
        templateHtml.find('[data-key="deposit-fee"]').html(depositFee + '%');
        templateHtml.find('[data-key="transfer-fee"]').html(transferFee + '%');
        templateHtml.find('[data-key="withdrawal-lockup"]').html(withdrawalInterval + ' hours');
        templateHtml.find('[data-key="stake-min"]').html(minDepositAmount);
        templateHtml.find('[data-key="stake-max"]').html(maxStakeAmountText);
        templateHtml.find('[data-key="stake-max"]').attr('id', 'stake-max-' + stakingToken.symbol);
        templateHtml.find('[data-key="can-withdraw"]').html(canWithdrawText);
        templateHtml.find('[data-key="require-hold"]').html('0 ' + stakingToken.symbol);
        templateHtml.find('[data-key="earned-token"]').html(pendingRewardReadable + '<span class="p-staked-usd">$' + earningUSD.toFixed(2) + '</span>');
        templateHtml.find('[data-key="staked-token"]').html(stakedTokenReadable + '<span class="p-staked-usd">$' + stakingUSD.toFixed(2) + '</span>');
        templateHtml.find('[data-key="earned-token"]').attr('id','earned-token-'+sousId);
        templateHtml.find('[data-key="staked-token"]').attr('id','staked-token-'+sousId);
        templateHtml.find('[data-key="lbl-earned"]').html(earningToken.symbol+ ' earned');
        templateHtml.find('[data-key="lbl-staked"]').html(stakingToken.symbol+ ' staked');
        templateHtml.find('[data-key="earned-usd"]').html('('+earningUSD.toFixed(2)+ ' USD)');
        templateHtml.find('[data-key="staked-usd"]').html('('+stakingUSD.toFixed(2)+ ' USD)');
        templateHtml.find('[data-key="lbl-total-staked"]').html(totalStaked + ' ' + stakingToken.symbol);
        templateHtml.find('[data-key="total-staked-usd"]').html('('+totalStakedUSD.toFixed(2)+ ' USD)');
        templateHtml.find('[data-key="max-stake-per-user"]').html(maxStakedAmount+' ' +stakingToken.symbol);
        templateHtml.find('[data-key="ends-block"]').html(endBlock + ' blocks');
        templateHtml.find('[data-key="btn-enable"]').attr('id','btnEnable-'+stakingToken.symbol);
        templateHtml.find('[data-key="btn-enable"],[data-key="btn-stake"]').attr('data-contract' , stakingToken.address[selectedChain.chainId]);
        templateHtml.find('[data-key="btn-enable"]').attr('data-spender_contract' , contractAddress);
        templateHtml.find('[data-key="btn-stake"]').attr('id','btnStake-'+stakingToken.symbol);
        templateHtml.find('[data-key="btn-stake"]').attr('data-contract' , contractAddress);
        templateHtml.find('[data-key="btnStakingConfirm"]').attr('data-sousid' , sousId);
        templateHtml.find('[data-key="btnStakingConfirm"]').attr('data-spender' , stakingToken.address[selectedChain.chainId]);
        templateHtml.find('[data-key="data-stake"]').attr('data-stake_min' , minDepositAmount);
        templateHtml.find('[data-key="stake-details"]').attr('id','stake-details-'+stakingToken.symbol);
        templateHtml.find('[data-key="btn-plus"]').attr('data-contract' , contractAddress);
        templateHtml.find('[data-key="btn-minus"]').attr('data-contract' , contractAddress);
        templateHtml.find('[data-key="btnStakingConfirm"]').attr('data-contract' , contractAddress);
        templateHtml.find('[data-key="stake-symbol"]').html(stakingToken.symbol);
        templateHtml.find('[data-key="stake-balance"]').html(stakedToken);
        templateHtml.find('[data-key="user-balance"]').html(balanceOf);
        templateHtml.find('[data-key="btnStakingConfirm"]').attr('data-decimals',stakingTokenDecimals);
        templateHtml.find('[data-key="btnStakingConfirm"]').attr('data-symbol', stakingToken.symbol);
        templateHtml.find('[data-key="btnUnStakingConfirm"]').attr('data-contract' , contractAddress);
        templateHtml.find('[data-key="btnUnStakingConfirm"]').attr('data-decimals',stakingTokenDecimals);
        templateHtml.find('[data-key="btnUnStakingConfirm"]').attr('data-symbol', stakingToken.symbol);
        templateHtml.find('[data-key="btnUnStakingConfirm"]').attr('data-sousid', sousId);
        templateHtml.find('[data-key="harvest-symbol"]').html(earningToken.symbol);
        templateHtml.find('[data-key="harvest-usd"]').html(earningUSD.toFixed(2)+ ' USD');
        templateHtml.find('[data-key="harvest-amount"]').html(pendingReward);
        templateHtml.find('[data-key="harvest-amount"]').attr('id','harvest-amount-'+sousId);
        templateHtml.find('[data-key="btn-harvest-confirm"]').attr('data-symbol', earningToken.symbol);
        templateHtml.find('[data-key="btn-harvest-confirm"]').attr('data-contract' , contractAddress);
        templateHtml.find('[data-key="stake-amount"]').attr('id','stake-amount_'+sousId);
        templateHtml.find('[data-key="unstake-amount"]').attr('id','unstake-amount_'+sousId);
        templateHtml.find('[data-key="contract-details"]').attr('href',explorerURL+'address/'+contractAddress);
        templateHtml.find('[data-key="addMM"]').attr('data-contract', stakingToken.address[selectedChain.chainId]);
        templateHtml.find('[data-key="addMM"]').attr('data-decimals', stakingTokenDecimals);
        templateHtml.find('[data-key="addMM"]').attr('data-icon', iconStaking);
        templateHtml.find('[data-key="addMM"]').attr('data-symbol', stakingToken.symbol);

        if(balanceOf == 0) {
            templateHtml.find('[data-key="btnStakingConfirm"]').attr('disabled', 'disabled');    
        }

        if(stakedToken == 0) {
            templateHtml.find('[data-key="btnUnStakingConfirm"]').attr('disabled', 'disabled');    
        }

        if(date_start && date_start != 'Invalid Date') {
            // Create date to this format: Jan. 1st 2024
            const dateOptions = { year: 'numeric', month: 'short', day: 'numeric' };
            const formattedDate = date_start.toLocaleDateString('en-US', dateOptions);

            templateHtml.find('[data-key="available-from"]').html(formattedDate);
        } else {
            templateHtml.find('[data-key="available-from"]').html('');
        }

        if(date_end && date_end != 'Invalid Date') {
            // Create date to this format: Jan. 1st 2024
            const dateOptions = { year: 'numeric', month: 'short', day: 'numeric' };
            const formattedDate = date_end.toLocaleDateString('en-US', dateOptions);

            templateHtml.find('[data-key="available-till"]').html(formattedDate);
        } else {
            templateHtml.find('[data-key="available-till"]').html('');
        }

        registerPoolCardEvents($(templateHtml));

        if(wallet_address == null) {
            $('.btn-connect').show();
            $('[data-poolid="' + sousId + '"]').find('.btn-enable').hide();
        } else {
            $('.btn-connect').hide();

            if(allowance > 0 || stakedToken > 0) {
                $('[data-poolid="' + sousId + '"]').find('.btn-enable').hide();
                $('[data-poolid="' + sousId + '"]').find('.harvest-section').show();
                $('[data-poolid="' + sousId + '"]').find('.stake-details').show();
            } else {
                $('[data-poolid="' + sousId + '"]').find('.btn-enable').show();
                $('[data-poolid="' + sousId + '"]').find('.harvest-section').hide();
                $('[data-poolid="' + sousId + '"]').find('.stake-details').hide();
            }
        }

        if($('.no-pools-found').length > 0) {
            $('.no-pools-found').remove();
        }
    }
};

function toBn(value) {
    return ethers.BigNumber.from(value);
}

function calcPoolApr(stakingTokenPrice, rewardTokenPrice, totalStaked, tokenPerBlock) {
    const blockTime = 3;
    const blocksPerYear = (60 / blockTime) * 60 * 24 * 365;
    const totalRewardPricePerBlockYear = rewardTokenPrice * tokenPerBlock * blocksPerYear;
    const totalStakingTokenInPoolContract = stakingTokenPrice * totalStaked;

    const poolApr = (totalRewardPricePerBlockYear / totalStakingTokenInPoolContract) * 100;

    return isNaN(poolApr) || !isFinite(poolApr) ? null : poolApr;
}

function registerPoolCardEvents(cardEl) {
    // Add click event to the .btn-bottomsheet buttons which then removes .d-none from the parents child .bottom-sheet
    const btnBottomSheet = cardEl.find('.btn-bottomsheet');

    btnBottomSheet.each(function(i, btn){
        btn.addEventListener('click', () => {
            const bottomSheet = btn.parentElement.parentElement.querySelector('.bottom-sheet')
            bottomSheet.classList.remove('d-none');
        })
    });

    // Add click event to the .btn-close-bottom-sheet buttons which then adds .d-none to the closest upwards element .bottom-sheet
    const btnCloseBottomSheet = cardEl.find('.btn-close-bottom-sheet');

    btnCloseBottomSheet.each(function(i, btn) {
        btn.addEventListener('click', () => {
            const bottomSheet = btn.closest('.bottom-sheet')
            bottomSheet.classList.add('d-none');
        })
    });

    // Add click event to .btn-do-stake button
    const btnDoStake = cardEl.find('.btn-do-stake');

    btnDoStake.each(function(i, btn) {
        btn.addEventListener('click', () => {
            const bottomSheet = $(btn).closest('.card').find('.staking-stake');
            bottomSheet.removeClass('d-none');
        })
    });

    // Add click event to .btn-do-stake button
    const btnDoUnstake = cardEl.find('.btn-do-unstake');

    btnDoUnstake.each(function(i, btn) {
        btn.addEventListener('click', () => {
            const bottomSheet = $(btn).closest('.card').find('.staking-unstake');
            bottomSheet.removeClass('d-none');
        })
    });

    // Add interaction to progressbar .progress
    const progressBar = cardEl.find('.progress');

    progressBar.each(function(i, bar) {
        bar.addEventListener('click', () => {
            // Find the position of the click in percent
            let clickPosition = event.offsetX / bar.offsetWidth * 100;
            // Set the progressbar width
            bar.querySelector('.progress-bar').style.width = clickPosition + '%';
        })
    });

    // Add click events to the .progress-step buttons and use its data-percent attribute to set the progressbar width
    const progressStep = cardEl.find('.progress-step');

    progressStep.each(function (i, step) {
        step.addEventListener('click', () => {
            let progressbar = $(this).closest('.staking-stake').find('.progress-bar');
            let progressbarUnstake = $(this).closest('.staking-unstake').find('.progress-bar');
            let percent = $(this).data('percent');
            
            progressbar.css('width', percent + '%');
            progressbarUnstake.css('width', percent + '%');

            let userBalance = $(this).closest('.staking-stake').find('.user-balance').text();
            let stakeBalance = $(this).closest('.staking-unstake').find('.stake-balance').text();

            let stake_amount = 0;
            
            if(userBalance != "") {
                stake_amount = userBalance * percent / 100;
                $(this).closest('.staking-stake').find('.stake-amount').val(stake_amount);
            }

            if(stakeBalance != "") {
                stake_amount = ethers.utils.parseEther(stakeBalance.toString());
                stake_amount = stake_amount.mul(percent);
                stake_amount = stake_amount.div(100);
                stake_amount = ethers.utils.formatEther(stake_amount);

                $(this).closest('.staking-unstake').find('.unstake-amount').val(stake_amount.toString());
            }
        });
    });

    // Add click event to .btn-do-stake button
    const btnDoHarvest = cardEl.find('.btn-do-harvest');

    btnDoHarvest.each(function(i, btn) {
       btn.addEventListener('click', () => {
           const bottomSheet = $(btn).closest('.card').find('.staking-harvest');
           bottomSheet.removeClass('d-none');
       })
    });

    //Add click event to connect button 
    const btnConnect = cardEl.find('.btn-connect');

    btnConnect.each(function(i, btn) {
        btn.addEventListener('click', () => {
            web3Modal.openModal();
        })
    });
}

const toggleSwapModeContent = document.querySelectorAll('.toggle-swap-mode')
toggleSwapModeContent.forEach(btn => {
    btn.addEventListener('click', () => {
        const swapModeContent = btn.parentElement.parentElement.querySelector('.swap-mode-content');
        swapModeContent.classList.toggle('hidden');
        const icon = btn.querySelector('i')
        icon.classList.toggle('fa-chevron-down');
        icon.classList.toggle('fa-chevron-up')
    })
});

const btnSwapMode = document.querySelectorAll('.btn-swap-mode')
btnSwapMode.forEach(btn => {
    btn.addEventListener('click', () => {
        btnSwapMode.forEach(btn => {
            btn.classList.remove('active')
        })
        btn.classList.add('active')
    })
});

const tokenSelector = document.querySelectorAll('.token-selector .list-group-item');
tokenSelector.forEach(btn => {
    btn.addEventListener('click', () => {
        const tokenSelector = btn.parentElement.parentElement.parentElement.querySelector('.token-selector')
        tokenSelector.classList.add('d-none')
    })
});

const btnSelectToken = document.querySelectorAll('.btn-select-token');
btnSelectToken.forEach(btn => {
    btn.addEventListener('click', () => {
        const tokenSelector = btn.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.querySelector('.token-selector')
        tokenSelector.classList.remove('d-none')
    })
});

const btnClosePopOver = document.querySelectorAll('.btn-close-pop-over');
btnClosePopOver.forEach(btn => {
    btn.addEventListener('click', () => {
        const popOver = btn.parentElement.parentElement.parentElement.querySelector('.pop-over')
        popOver.classList.add('d-none')
    })
});

const bottomSheet = document.querySelectorAll('.bottom-sheet');
bottomSheet.forEach(btn => {
    btn.addEventListener('click', (event) => {
        const bottomSheet = btn.parentElement.querySelector('.bottom-sheet')
        bottomSheet.classList.add('d-none')
    })
});

const btnConnect = document.querySelectorAll('.btn-connect');
btnConnect.forEach(btn => {
    btn.addEventListener('click', () => {
        document.querySelector('[data-testid=component-big-button]').click();
    })
});

$(function() {
    enableStaking();

    $('#staking-search').on('keyup', function() {
        var value = $(this).val().toLowerCase();
        $('.staking-cards > div').filter(function() {
            $(this).toggle($(this).find('.card-header').text().toLowerCase().indexOf(value) > -1)
        })
    });

    $('#staking-search').on('search', function() {
        var value = $(this).val().toLowerCase();
        $('.staking-cards > div').filter(function() {
            $(this).toggle($(this).find('.card-header').text().toLowerCase().indexOf(value) > -1)
        })
    });
});

function enableStaking() {
    $('.staking-card .btn-enable').click(function() {})
}

// listening for account changes
watchAccount(account => {
    if(wallet_address != null) {
        if(account.isConnected) {
            wallet_address = account.address;
            safeswap.fetchSafeSwapPools();
        } else {
            if(account.isConnecting == false) {
                wallet_address = undefined
            }
        }
    } else {
        wallet_address = account.address ? account.address : null;
        safeswap.fetchSafeSwapPools();
    }
});

// Approve Function Call
$(document).on("click", ".btn-enable", async function() {
    let contract = $(this).data("contract")
    let spender = $(this).data("spender_contract")
    let btnID = $(this).attr('id')    
    btnID = btnID.replace('btnEnable-', '');
    
    const { hash } = await writeContract({
        address: contract,
        abi: stakingABI,
        functionName: 'approve',
        args: [spender,approveAmount],
    });

    const data = await waitForTransaction({
        hash,
    });
        
    if(data.status == 'success') {
        CoreModal.make('Contract enabled', 'You can now stake in the <strong>' + btnID + '</strong> pool!', [{ text: 'Close', callback: function(){ CoreModal.hide(); } }]).show();
        safeswap.fetchSafeSwapPools();        
    } else {
        CoreModal.make('Warning!', 'You haven\'t enabled <strong>' + btnID + '</strong> yet. Try again.', [{ text: 'Close', callback: function(){ CoreModal.hide(); } }]).show();
    }
});

// Stake Function Call
$(document).on("click", ".btnStake", async function() {
    let contract = $(this).data("contract")
    let spender = $(this).data("spender")
    let btnID = $(this).data('symbol')    
    let decimals = $(this).data('decimals')    
    let id = $(this).data('sousid')    
    let depositAmount = $('#stake-amount_'+id).val();

    depositAmount = ethers.utils.parseUnits(depositAmount, decimals);
    
    const staking_Contract = {
        address: spender,
        abi: stakingABI,
    }

    const contract_data = await readContracts({
        contracts: [
            {
                ...staking_Contract,
                functionName : 'allowance',
                args : [wallet_address,contract]
            },
            {
                ...staking_Contract,
                functionName : 'balanceOf',
                args : [wallet_address]
            },
        ]
    });

    const allowance = contract_data[0].result;
    var balanceOf = contract_data[1].result;

    balanceOf = ethers.BigNumber.from(balanceOf);

    if(depositAmount.gt(balanceOf)) {
        CoreModal.make('Warning', 'Your balance is lower then the requested amount to stake.', [{ text: 'Close', callback: function(){ CoreModal.hide(); } }]).show();
        return false;
    }

    const stakingContract = {
        address: contract,
        abi: ABI,
    }

    const staking_limit_data = await readContracts({
        contracts: [
            {
                ...stakingContract,
                functionName : 'poolLimitPerUser',
            },
        ]
    });

    let poolLimitPerUser = staking_limit_data[0].result;

    poolLimitPerUser = ethers.BigNumber.from(poolLimitPerUser);

    if(depositAmount.gt(poolLimitPerUser)) {
        CoreModal.make('Warning', 'The amount you requested to stake, exceeds the pool limit.', [{ text: 'Close', callback: function(){ CoreModal.hide(); } }]).show();
        return false;
    }

    const { hash } = await writeContract({
        address: contract,
        abi: ABI,
        functionName: 'deposit',
        args: [depositAmount.toString()]
    });

    const data = await waitForTransaction({
        hash,
    });

    if(data.status == 'success') {
        CoreModal.make('Success', 'Your <strong>' + btnID + '</strong> funds have been staked in the pool.', [{ text: 'Close', callback: function(){ CoreModal.hide(); } }]).show();
        safeswap.fetchSafeSwapPools();
    } else {
        CoreModal.make('Failed', 'Your <strong>' + btnID + '</strong> funds have not been staked because of an unknown error, please try again.', [{ text: 'Close', callback: function(){ CoreModal.hide(); } }]).show();
    }
});

// Unstake Function Call
$(document).on("click", ".btnUnStake", async function() {
    let contract = $(this).data("contract");
    let btnID = $(this).data('symbol');
    let decimals = $(this).data('decimals');
    let id = $(this).data('sousid');

    const stakingContract = {
        address: contract,
        abi: ABI,
    }    
    
    const data2 = await readContracts({
        contracts: [
            {
                ...stakingContract,
                functionName : 'userInfo',
                args: [wallet_address],
            },
        ]
    });

    let stakedToken = 0;

    stakedToken = data2[0].result[0];
    stakedToken = ethers.BigNumber.from(stakedToken);

    let unstakeAmount = $('#unstake-amount_'+id).val();

    unstakeAmount = ethers.utils.parseUnits(unstakeAmount, decimals);
    
    if(unstakeAmount.gt(stakedToken)) {
        unstakeAmount = stakedToken;
    }

    const { hash } = await writeContract({
        address: contract,
        abi: ABI,
        functionName: 'withdraw',
        args: [unstakeAmount.toString()]
    });

    const data = await waitForTransaction({
        hash,
    });
   
    if(data.status == 'success') {
        CoreModal.make('Success', 'Your <strong>' + btnID + '</strong> tokens have been unstaked to your wallet.', [{ text: 'Close', callback: function(){ CoreModal.hide(); } }]).show();
        safeswap.fetchSafeSwapPools();
    } else {
        CoreModal.make('Failed', 'Your <strong>' + btnID + '</strong> tokens have not been unstaked to your wallet because of an unknown error. Please try again.', [{ text: 'Close', callback: function(){ CoreModal.hide(); } }]).show();
    }
})

// Harvest Function Call
$(document).on("click", ".btnHarvest", async function () {
    let contract = $(this).data("contract");
    let btnID = $(this).data('symbol');
    let harvestAmount = 0;

    const { hash } = await writeContract({
        address: contract,
        abi: ABI,
        functionName: 'deposit',
        args: [harvestAmount]
    });

    const data = await waitForTransaction({
        hash,
    });
    
    if(data.status == 'success') {
        CoreModal.make('Success', 'Your <strong>' + btnID + '</strong> earnings have been harvested to your wallet.', [{ text: 'Close', callback: function(){ CoreModal.hide(); } }]).show();
        safeswap.fetchSafeSwapPools();           
    } else {
        CoreModal.make('Failed', 'Your <strong>' + btnID + '</strong> earnings have not been harvested to your wallet because of an unknown error. Please try again.', [{ text: 'Close', callback: function(){ CoreModal.hide(); } }]).show();
    }
});

$('#btnFinished').click(function () {
    $('#btnLive').removeClass('active');
    $(this).addClass('active');

    showLivePools = false;

    safeswap.fetchSafeSwapPools();
});

$('#btnLive').click(function () {
    $('#btnFinished').removeClass('active');
    $(this).addClass('active');

    showLivePools = true;

    safeswap.fetchSafeSwapPools();
});

$('#btnShowAll').click(function () {
    $('#btnShowStaked').removeClass('active');
    $(this).addClass('active');
    showAll = true;
    showStaked = false;

    safeswap.fetchSafeSwapPools();
});

$('#btnShowStaked').click(function() {
    $('#btnShowAll').removeClass('active');
    $(this).addClass('active');
    showStaked = true;
    showAll = false;

    safeswap.fetchSafeSwapPools();
});

$('.btn-staking-reload').click(function () {
    const icon = $(this).find('i');

    icon.removeClass('fa-rotate-right');
    icon.addClass('fa-spinner fa-spin');

    safeswap.fetchSafeSwapPools(() => {
        icon.removeClass('fa-spinner fa-spin');
        icon.addClass('fa-rotate-right');
    });
});

function updateNumber(element, number, earningUSD) {
    $(element).prop('counter', 0).animate({
        counter: number
    },
    {
        duration: 500,
        step: function (now) {
            $(this).html(now.toFixed(7) + '<span class="p-staked-usd">$' + earningUSD.toFixed(2) + '</span>');
        }
    });
};

async function checkReward() {
    $('.sousId').each(async function(i, obj) {
        var contractAddress =  $(this).data("contract")
        var stakingTokenDecimals =  $(this).data("stakingdecimals")
        var earningTokenDecimals =  $(this).data("earningdecimals")

        var id = $(this).data("id");

        if(wallet_address != null) {
            const stakingContract = {
                address: contractAddress,
                abi: ABI,
            }

            const data = await readContracts({
                contracts: [
                    {
                        ...stakingContract,
                        functionName : 'pendingReward',
                        args: [wallet_address],
                    },
                    {
                        ...stakingContract,
                        functionName : 'userInfo',
                        args: [wallet_address],
                    }
                ]
            });

            let token_addresses = sausIdToAddress[id];
            
            const stakingTokenPrice = tokenPrices[token_addresses.staking];
            const earningTokenPrice = tokenPrices[token_addresses.earning];

            let pendingReward = 0;
            let pendingRewardReadable = 0;
            let stakedToken = 0;
            let stakedTokenReadable = 0;

            stakedToken = ethers.utils.formatUnits(data[1].result[0].toString(), stakingTokenDecimals);
            stakedTokenReadable = Math.floor(stakedToken * 1000) / 1000;

            pendingReward = ethers.utils.formatUnits(data[0].result.toString(), earningTokenDecimals);
            pendingRewardReadable = Math.floor(pendingReward * 10000000) / 10000000;

            let earningUSD = pendingReward * earningTokenPrice;
            if(isNaN(earningUSD)) { 
                earningUSD = 0; 
            }

            let stakingUSD = stakedToken * stakingTokenPrice;
            if(isNaN(stakingUSD)) { 
                stakingUSD = 0; 
            }

            $('#earned-token-'+id).html(pendingRewardReadable)
            $('#staked-token-'+id).html(stakedTokenReadable + '<span class="p-staked-usd">$' + stakingUSD.toFixed(2) + '</span>')
            $('#harvest-amount-'+id).html(pendingReward);

            $('#earned-token-'+id).each(function() {
                let number = $(this).text();
                updateNumber(this, number, earningUSD);
            });
        }
    });
}

setInterval(checkReward, 60000);

$(document).on('click', '.addMM', async function () {
    const tokenAddress = $(this).data("contract")
    const tokenSymbol = $(this).data("symbol")
    const tokenDecimals = $(this).data("decimals");
    const tokenImage = $(this).data("icon");

    try {
        const wasAdded = await window.ethereum.request({
            method: 'wallet_watchAsset',
            params: {
                type: 'ERC20',
                options: {
                    address: tokenAddress,
                    symbol: tokenSymbol,
                    decimals: tokenDecimals,
                    image: tokenImage
                }
            }
        });

        if(wasAdded) {
            CoreModal.make('Success', '<strong>' + tokenSymbol + '</strong> has been added to Metamask.', [{ text: 'Close', callback: function() { CoreModal.hide(); } }]).show();
        }
    } catch (error) {
        console.log(error);
    }
});

$(function () {
    $('.pool-sort').click(function () {
        let sort_key = $(this).data('sort');

        $(this).closest('.dropdown').find('.dropdown-toggle').html('Sort by: ' + $(this).text());

        if(sort_key == 'name') {
            $('.pool-sort').removeClass('active');
            $(this).addClass('active');

            $('.staking-cards > div').sort(function(a, b) {
                return $(a).find('.card-header').text().toLowerCase().localeCompare($(b).find('.card-header').text().toLowerCase());
            }).appendTo('.staking-cards');
        } else if(sort_key == 'earned') {
            $('.pool-sort').removeClass('active');
            $(this).addClass('active');
            $('.staking-cards > div').sort(function(a, b) {
                // Match by .earned-amount (contains the number)
                return $(b).find('.earned-amount').text().toLowerCase().localeCompare($(a).find('.earned-amount').text().toLowerCase());
            }).appendTo('.staking-cards');
        } else if(sort_key == 'staked') {
            $('.pool-sort').removeClass('active');
            $(this).addClass('active');
            $('.staking-cards > div').sort(function(a, b) {
                return $(b).find('.staked-amount').text().toLowerCase().localeCompare($(a).find('.staked-amount').text().toLowerCase());
            }).appendTo('.staking-cards');
        } else if(sort_key == 'end-date') {
            $('.pool-sort').removeClass('active');
            $(this).addClass('active');
            $('.staking-cards > div').sort(function(a, b) {
                return $(b).find('[data-key="available-till"]').text().toLowerCase().localeCompare($(a).find('[data-key="available-till"]').text().toLowerCase());
            }).appendTo('.staking-cards');
        } else if(sort_key == 'apr') {
            $('.pool-sort').removeClass('active');
            $(this).addClass('active');
            $('.staking-cards > div').sort(function(a, b) {
                return $(b).find('[data-key="apr"]').text().toLowerCase().localeCompare($(a).find('[data-key="apr"]').text().toLowerCase());
            }).appendTo('.staking-cards');
        }
    });
});

function debug(line, data)
{
    let data_str = '';
    if(typeof data != 'undefined'){
        data_str = '<br/><code>' + JSON.stringify(data) + '</code>';
    }

    let console_el;
    if (typeof visual_debug != 'undefined' && visual_debug) {
        if($('.console').length == 0) {
            console_el = $('<div class="console"></div>');
            $('body').append(console_el);
        } else {
            console_el = $('.console');
        }

        let cl_line = $('<div class="line">' + line + data_str + '</div>');
        console_el.append(cl_line);
    } else {
        //console.log(line, data);
    }
}